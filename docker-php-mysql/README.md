# Información para usar la app

## Todo esta listo simplemente debes seguir los 2 passos para poder usar la aplicación

1. Situarse en la carpeta docker-php-mysql

2. Docker compose up -d

```
docker-compose up -d
```

[Admin portal](http://localhost/admin_portal.php)

[Login user](http://localhost)

[Update password User](http://localhost/update_password.php) Una vez hecho login desde la pàgina puedes cerrar session y tambíen pudes cambiar la contrasenya. :)

[Mi idealista](http://localhost/miidealista.php) tabla mi idealista

Una vez hecho el login arriba de la pàgina veras las opciones de cerrar session y update password

![Info image](info_readme.png)

# ------------------------------------------------------------------------------------------------------------


# Información para el programador

# Docker: PHP & MySQL

Instala rápidamente un ambiente de desarrollo local para trabajar con [PHP](https://www.php.net/) y [MySQL](https://www.mysql.com/) utilizando [Docker](https://www.docker.com). 

Utilizar *Docker* es sencillo, pero existen tantas imágenes, versiones y formas para crear los contenedores que hacen tediosa esta tarea. Este proyecto ofrece una instalación rápida, con versiones estandar y con la mínima cantidad de modificaciones a las imágenes de Docker. Viene configurado con  `PHP 7.3` y `MySQL 5.7`.

## Requerimientos

* [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Configurar el ambiente de desarrollo

Puedes utilizar la configuración por defecto, pero en ocasiones es recomendable modificar la configuración para que sea igual al servidor de producción. La configuración se ubica en el archivo `.env` con las siguientes opciones:

* `PHP_VERSION` versión de PHP ([Versiones disponibles de PHP](https://github.com/docker-library/docs/blob/master/php/README.md#supported-tags-and-respective-dockerfile-links)).
* `PHP_PORT` puerto para servidor web.
* `MYSQL_VERSION` versión de MySQL([Versiones disponibles de MySQL](https://hub.docker.com/_/mysql)).
* `MYSQL_USER` nombre de usuario para conectarse a MySQL.
* `MYSQL_PASSWORD` clave de acceso para conectarse a MySQL.
* `MYSQL_DATABASE` nombre de la base de datos que se crea por defecto.

# Para hacer tener la app disponible simplemente en el que contenga el docker-compose.yml realizamos el siguiente comando:
```
docker-compose up -d
```
## Comandos disponibles

Una vez instalado, se pueden utilizar los siguiente comandos:

```zsh
docker-compose start    # Iniciar el ambiente de desarrollo
docker-compose stop     # Detener el ambiente de desarrollo
docker-compose down     # Detener y eliminar el ambiente de desarrollo.
```

## Estructura de Archivos

* `/docker/` contiene los archivos de configuración de Docker.
* `/www/` carpeta para los archivos PHP del proyecto.

## Accesos

### Web

* http://localhost/

### Base de datos

Existen dos dominios para conectarse a base de datos.

* `mysql`: para conexión desde los archivos PHP.
* `localhost`: para conexiones externas al contenedor.


Password of root mysql, run as command line(because in each execution the admin pass change)
```
docker run --name=docker-mysql mysql/mysql-server:latest

```
And then search the log password generated to enter in admin mode on mysql.

To fix this error "ERROR 1820 (HY000): You must reset your password using ALTER USER statement before executing this statement.": 


```
ALTER USER 'root'@'localhost' IDENTIFIED  BY 'MYSQL_ROOT_PASSWORD';
```


Type the following commands if you have MySQL 5.7.6 and later or MariaDB 10.1.20 and later:

```
ALTER USER 'user-name'@'localhost' IDENTIFIED BY 'NEW_USER_PASSWORD';
FLUSH PRIVILEGES;
```

If ALTER USER statement doesn’t work for you, you can modify the user table directly:

```
UPDATE mysql.user SET authentication_string = PASSWORD('NEW_USER_PASSWORD')
WHERE User = 'user-name' AND Host = 'localhost';
FLUSH PRIVILEGES;
```