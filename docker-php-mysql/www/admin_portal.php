<head>
    <link rel=stylesheet href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<h1>Admin portal</h1><br>
<script>
    function EnviarPost(accion,id) {
        document.forms.formulario.accion.value=accion;
        document.forms.formulario.id.value=id;
        document.forms.formulario.submit();
    }
</script>
<?php
    $mysqli = new mysqli("mysql", "root", "root", "xarxatallers");

    /* comprobar la conexión */
    if (mysqli_connect_errno()) {
        printf("Falló la conexión: %s\n", mysqli_connect_error());
        exit();
    }
    
    $consulta = "select id,nombre,apellido,edad,mail,direccion from usuaris";
    
    if ($resultado = $mysqli->query($consulta)) {
        echo "<table border=2>";
        /* obtener el array de objetos */
        while ($fila = $resultado->fetch_row()) {
            //printf ("%s (%s)\n", $fila[0], $fila[1]);
            echo "<tr><td>$fila[0]<td>$fila[1]<td>$fila[2]<td>$fila[3]<td>$fila[4]<td>$fila[5]<td> <button onclick=EnviarPost('borrado',$fila[0])>Borrar</button></td>";
        }
        echo "</table><br><br>";
        /* liberar el conjunto de resultados */
        $resultado->close();
    }
    
    /* cerrar la conexión */
    $mysqli->close();
?>

<form name="formulario" action="admin.php" method="post">
    Nombre : <input type="text" class="search" name="nombre" required/><br><br>
    <div id="result"></div>
    Apellido: <input type="text" name="apellido" required/><br><br>
    Edad: <input type="text" name="edad" required/><br><br>
    Mail: <input type="text" name="mail" required/><br><br>
    Direccion: <input type="text" name="direccion" required/><br><br>
    Password: <input type="password" name="password" required/><br><br>
    <input type="hidden" name="accion"/>
    <input type="hidden" name="id"/>
    <input type="submit" value="Insertar"/>
</form>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
    $(function() {
        $(".search").keyup(function() {
            
            searchid = $(this).val();
            var dataString = 'auto='+ searchid;
            if (searchid!="")
            {
                $.ajax({
                    type: "POST",
                    url: "dp.php",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        $("#result").html(html).show();
                    }
                });
            }
        });
    });
</script>
