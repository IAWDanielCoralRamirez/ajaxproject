create table usuaris (
    id int auto_increment not null,
    nombre varchar(20) not null,
    apellido varchar(20) not null,
    edad int not null,
    mail varchar(30) not null,
    direccion varchar(30) not null,
    passwd varchar(30) not null,
    primary key(id)
) engine=InnoDB charset=utf8 auto_increment=0;